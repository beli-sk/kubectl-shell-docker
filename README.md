kubectl & shell
===============

Alpine based image with _bash_, _kubectl_, _Helm_ and some CLI tools.

Installed tools
---------------

- bash
- curl
- gettext
- helm
- kubectl
- wget

Automatic build
---------------

The image is built automatically and transparently with [Gitlab CI
pipeline][pipeline], pushed to [Docker Hub registry][registry] and can be
pulled using command

    docker pull beli/kubectl-shell

or if you'd prefer to build it yourself from the source repository, clone this
repository locally and run:

    docker build -t beli/kubectl-shell .

[pipeline]: https://gitlab.com/beli-sk/kubectl-shell-docker/pipelines
[registry]: https://hub.docker.com/r/beli/kubectl-shell/

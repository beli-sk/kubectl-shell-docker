FROM alpine
MAINTAINER Michal Belica <code@beli.sk>
ARG helm_release=latest
ARG kubectl_release=stable
RUN apk --no-cache add bash curl gettext wget \
	&& if [[ "$kubectl_release" == "stable" ]] ; then \
		kubectl_release=`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt` ; fi \
	&& curl -Lo /usr/local/bin/kubectl \
	"https://storage.googleapis.com/kubernetes-release/release/${kubectl_release}/bin/linux/amd64/kubectl" \
	&& chown root:root /usr/local/bin/kubectl && chmod +x /usr/local/bin/kubectl\
	&& if [[ "$helm_release" == "latest" ]] ; then \
		helm_release=$( wget https://github.com/helm/helm/releases/latest --server-response -O /dev/null 2>&1 \
		| awk '/^  Location: /{DEST=$2} END{ print DEST}' | grep -oE "[^/]+$" ) ; fi \
	&& echo https://get.helm.sh/helm-${helm_release}-linux-amd64.tar.gz \
	&& curl -L https://get.helm.sh/helm-${helm_release}-linux-amd64.tar.gz \
		| tar -C /usr/local/bin --strip-components=1 -zx linux-amd64/helm \
	&& chown root:root /usr/local/bin/helm && chmod +x /usr/local/bin/helm
CMD ["/bin/bash"]
